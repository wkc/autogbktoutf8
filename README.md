# python批量转换GBK编码文件到UTF8

网上经常下载到使用GBK编码的代码，如果使用sublime之类的编辑器经常会乱码，win下notepad++对GBK支持很优秀，但是不跨平台。所有一般会事先转化为UTF-8编码再打开。

目前shell下可以使用的编码转换工具包括enca，convmv，iconv。其中enca，convmv大部分发行版必须自己安装

- enca用法：`find -type f | xargs enca -L zh_CN -x UTF-8` 把当前目录所有文本文件转化到UTF-8编码
- convmv用法： `convmv -f GBK -t UTF-8 --notest utf8 filename`  
- iconv用法： `iconv -f GBK -t UTF-8 file1 -o file2`

enca可以智能跳过不需要转换的文件，iconv在源文件已经是UTF-8编码的时候还会继续转换，然后会出现乱码而且无法还原，所以在没有备份文件的情况下慎用iconv命令。
python有一个模块chardet可以检测一段未知文本的编码，用这个模块可以很轻易的山寨出enca的编码检测功能。于是写了一个小脚本。
测试了下速度比enca慢了很多，主要时间花在chardet检测编码上，尽管已经为大文件优化了。
不过python的好处是可以跨平台，如果在windows下的话用enca这样的工具会很麻烦(cygwin?)。
